# Matterscope 2.0
This is the second version of the Matterscope project by Inés Burdiles, in collaboration with Josefina Nano's Phygital Awakenings

## Concept
The Matterscope, is a device, that in analogy to Telescopes and Microspoes, situates the human body to enable the observation of biomaterials through the senses. The general aim of the project is to enable and register interactions between the human body and another body, in this case biomaterial, that is being carefully observed and registered in its first days for shrinkages and deformations.

## Purpose 
The matterscope is a delimited space that situates the observed matter and the observer. The observed matter and the observer have a sensor to measure body temperature. The space is being measured for environmental temperature and humidity, all these sensors generate data that will later be crossed with a visual registration of the interaction. Specifically, throught the temperature log, the device registrates when the human body comes into contact with the non-human body through the sense of touch, and how this affects the matter.

## Plan & Execution of the Second Iteration of the Project
The aim was to develop a second Matterscope that could be assembled and dissassembled, to transport to another location or Lab to continue with the observation of different recipes of Biomaterials and their processes of shrinkages and deformations, to make in the future a vocabulary of deformations. The strategy was to design a CNC cut press-fit wooden structure, using the same 9mm plywood and kerf tolerances used for the Biomaterial Toolbox designed for Challenge 2. Although the Matterscope 2.0 structure differentiates because it has inner and outer pieces that hold in place the translucent skin. This is something that the first prototype did not consider. Also, the cuts consider space for shelving different samples, which is something that was done manually on the first prototype, as one of the aspects that the research itself started to demand. 

## Design & Fabrication
Design / 3D Models:
- Image of the structure designed in Rhinocerose. The method of getting the pieces from a 3D model, secures that the cut-ins and cut-outs are done in the correct place for smooth assembly. The model also shows the horizontal, inner and outer pieces differentiated by colors, this should help identification in the assembly process. For Assembly, the only tool needed is a hammer, and tape to fix the translucent 1x1cm grid membrane to the frames.
![fabric](images/Rhino1-general.JPG)

Fabrication / Hand's on
- Structure and Grid
The structure was built using 9mm plywood board cut in CNC for press-fit. Remaining strategy from the first prototype the whole structure was given its dimension according to the A3 size, from the printing of the 1x1 cms grid, to facilitate this side and top panels being interchangeble, to plain translucent white for example. Which would make the matterscope also a functional lightbox. For the simple registration of future samples.
![fabric](images/Rhino2-despiece.JPG)

- Assembly
The assembly is guided with the differentiation of colors in the model. The sequence would be, starting from the left side if you are facing the open side of the Matterscope, this is looking from the side of the slab that comes out 8cms towards the front, in the base. You start by placing the inner left vertical support on the floor and press-fitting the horizontal base and the inner horizontal top. Then you slide the back inner panel, to then press-fit the inner right vertical support. Once the inner structure is complete, the translucent grid should be placed. This can be donde with tape or glue, depending on the fragility and permanence of the membrane. After the membrane is firmly tensed and placed, the outer supports are press-fitted to the structure, starting again by placing the left outer vertical support, then the outer right vertical support, the outer top horizontal support, and at the end the outer back vertical support. All the left and right support vertical pieces, inner and outer, have a handle for carrying purposes, this was also added in this iteration as something that use showed was needed in the first prototype.
![fabric](images/Rhino3-colores.JPG)

- Cooking agar-agar biomaterial
Agar Agar Recipe:
• 300 ml of water
• 10 gr of agar agar
• 16 ml of glycerin
Instructions : Add the water and agar agar to the pot in cold. Stirring always, heat at medium temperature for 2
minutes. Raise the temperature for 2 more minutes. At minute 4 add the glycerin and optional filler. The mixture should change its texture, and start to solidify in minute 6. Pour exactly in that moment is just beginning to change texture into a dense liquid.

The mold used, is a 5mm width acrylic A4 size, covered with a oven paper only on one side of the mold, and a 5mm acrylic frame, that divides the mold in two equal parts, in which the same recipe is used to make two samples, that under the same conditions will develop differently due to human action, or other influential factors to be identified. This was hung from the structure with twisted wire that was bended to make a holding structure. 

## Coding Logic

We develop two parallel types of impact measurement.
Impact 01: measure the interaction between body temperature and biomaterial temperature when they come into contact. We used two temperature measurement sensors through touch (thermistor) connected to an arduino uno board and an LCD to display the values. This is mantained from the first prototype.

![fabric](images/arduinotermistor.jpg)

Impact 02: measure the ambient temperature and humidity to record the deformation of the biomaterial under different environmental conditions. We used ana DHT11 ambient temperature and humidity sensor, together with a visual registration of the biomaterial behavior. In difference to the first Matterscope, both the DHT11 sensor and the camera, were activated through a Raspberry Pi 4, that through a crontab script activates two python scripts, that every 30 minutes, take a photo of the materials and registers the humidity and temperature log on a google spreadsheet that can be followed remotely. This is the link to the google sheets:
https://docs.google.com/spreadsheets/d/1v_7Dl5Qtr8nZjphHpAYll19z9l5Wtc8H_2SFYyym_G4/edit?usp=sharing

## Files
The files you can find in this repository are:
- 3D model of CNC pieces in Rhinocerose
- Layout for CNC cutting in Rhinocerose
- nc files for screws on CNC
- nc files for cuts on CNC

## Final Prototype 

![fabric](images/generalyperspectiva.jpg)

![fabric](images/frontal.jpg)

![fabric](images/muestras.jpg)


## Iteration Process
We made mostly digital iterations for the hardware. And for the electronics several iterations. Starting with the definition of the python scripts for the DHT11 sensor, and separately for the camera. Both of this python scripts were to be executed through the crontab script, that sets a timelapse for the action. One of the most difficult things to do was to enable the google spreadsheet for the data that the Pi is generating. A non human user had to be created and authorized by google for the Pi. 

## Future Development
For future developments, the Matterscope will serve as a support for building a Vocabulary of deformations and shrinkages of different open source biomaterial recipes. The previous goal to build an on line repository for the temperature and humidity data was accomplished on this Iteration. It is still pending to get a repository for the images, coordinated in log time. The next step is to take the images from the Pi camera to see if it is possible to make video mapping over the material showing the process of deformation and shrinkages in real time.

## Link to individual pages
https://ines_burdiles.gitlab.io/sitio/single-fabweek17.html

